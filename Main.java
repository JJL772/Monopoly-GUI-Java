import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static javax.swing.JOptionPane.CLOSED_OPTION;

public class Main
{

    public static Square[] board = {
            new Square("GO!", "other", false, 60, 0),
            new Square("Med Ave", "property", true, 060, 1),
            new Square("Ori Ave", "property", true, 100, 2),
            new Square("Con Ave", "property", true, 120, 3),
            new Square("Chance", "chance", false, 0, 4),
            new Square("Read Rail", "property", true, 200, 5),
            new Square("Cha Pla", "property", true, 140, 6),
            new Square("Jail", "jail", false, 0, 7),
            new Square("Sta Ave", "property", true, 140, 8),
            new Square("Vir Ave", "property", true, 160, 9),
            new Square("Penn Rail", "property", true, 200, 10),
            new Square("Chance", "chance", false, 0, 11),
            new Square("Jam Pla", "property", true, 180, 12),
            new Square("Ten Ave", "property", true, 180, 13),
            new Square("Free Parking!", "other", false, 0, 14),
            new Square("New Ave", "property", true, 200, 15),
            new Square("Ken Ave", "property", true, 220, 16),
            new Square("Band Rail", "property", true, 200, 17),
            new Square("Chance", "chance", false, 0, 18),
            new Square("Elec Comp", "tax", true, 150, 19),
            new Square("Lux Tax", "tax", false, 250, 20),
            new Square("Jail", "jail", false, 0, 21),
            new Square("Ind Ave", "property", true, 240, 22),
            new Square("Ill Ave", "property", true, 240, 23),
            new Square("Chance", "chance", false, 0, 24),
            new Square("Shor Rail", "property", true, 200, 25),
            new Square("Atl Ave", "property", true, 260, 26),
            new Square("Ven Ave", "property", true, 260, 27),

    };
    private static Dice dice = new Dice();
    public static Pieces p1, p2;
    public static int p1Money, p2Money;
    private Square property;
    public static Scanner s = new Scanner( System.in );
    public static MonopolyGUI GUI;
    public static int turn = 1;

    public static void main(String[] args)
    {
        ArrayList<String> ops = new ArrayList<String>();
        ops.add("Man");
        ops.add("Hat");
        ops.add("Car");
        ops.add("Dog");
        while(true) {
            int res = JOptionPane.showOptionDialog(null, "Player 1, please choose a piece.", "Player Piece", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, ops.toArray(), "Man");
            if (res == CLOSED_OPTION){
                JOptionPane.showMessageDialog(null, "You must choose a piece!");
            }else{
                if(res == 0){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Man;
                    p.num = 1;
                    ops.remove("Man");
                    p1 = p;
                    break;
                }else if(res == 1){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Hat;
                    ops.remove("Hat");
                    p.num = 1;
                    p1 = p;
                    break;
                }else if(res == 2){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Car;
                    p1 = p;
                    p.num = 1;
                    ops.remove("Car");
                    break;
                }else if(res == 3){
                    Pieces p = new Pieces();
                    p.piece = MonopolyGUI.ePiece.Dog;
                    p.setOwner("Player1");
                    p.num = 1;
                    ops.remove("Dog");
                    p1 = p;
                    break;
                }else{
                    Pieces p = new Pieces();
                    p.piece = MonopolyGUI.ePiece.None;
                    p.setOwner("Player1");
                    p.num = 1;
                    p1 = p;
                    break;
                }
            }
        }
        while(true) {

            int res = JOptionPane.showOptionDialog(null, "Player 2, please choose a piece.", "Player Piece", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, ops.toArray(), "Man");
            if (res == CLOSED_OPTION){
                JOptionPane.showMessageDialog(null, "You must choose a piece!");
            }else{
                if(ops.get(res).equals("Man")){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Man;
                    p.num = 2;
                    p2 = p;
                    break;
                }else if(ops.get(res).equals("Hat")){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Hat;
                    p.num = 2;
                    p2 = p;
                    break;
                }else if(ops.get(res).equals("Car")){
                    Pieces p = new Pieces();
                    p.setOwner("Player1");
                    p.piece = MonopolyGUI.ePiece.Car;
                    p.num = 2;
                    p2 = p;
                    break;
                }else if(ops.get(res).equals("Dog")){
                    Pieces p = new Pieces();
                    p.piece = MonopolyGUI.ePiece.Dog;
                    p.setOwner("Player1");
                    p.num = 2;
                    p2 = p;
                    break;
                }else{
                    Pieces p = new Pieces();
                    p.piece = MonopolyGUI.ePiece.None;
                    p.setOwner("Player1");
                    p.num = 2;
                    p2 = p;
                    break;
                }
            }
        }
        GUI = new MonopolyGUI();
        GUI.Show();

        board[0].setp1Piece(p1);
        board[0].setp2Piece(p2);
        board[0].GUISquare.Piece1 = p1.piece;
        board[0].GUISquare.Piece2 = p2.piece;

        GUI.update();
        JOptionPane.showMessageDialog(null, "The game will now begin");

        while(true)
        {
            try
            {
                TimeUnit.SECONDS.sleep(1);
            }
            catch(Exception e)
            {
            }
        }
    }

    public static boolean canRoll(int ply)
    {
        Pieces[] s ={
                p1,
                p2,
        };
        Pieces p1 = s[ply-1];
        if(p1 != null)
        {
            if(p1.inJail)
            {
                int o1 = dice.getValue();
                int o2 = dice.getValue();
                if(o1==o2)
                    JOptionPane.showMessageDialog(null, "You rolled a double! You are now free, you can roll normally next turn!");
                else {
                    String[] ops = {
                            "Pay the fee",
                            "Wait for release",
                    };
                    int res = JOptionPane.showOptionDialog(null, "You didn't roll a double, but you will be released in 2 turns or, optionally, you can pay the fee of $300", "Jail",
                            2, JOptionPane.QUESTION_MESSAGE, null, ops, ops[1]);
                    if(res == 0){
                        if(p1.money >= 300){
                            p1.money -= 300;
                            p1.inJail = false;
                            p1.turns = 0;
                            JOptionPane.showMessageDialog(null,"You are free!!!!!");
                        }else{
                            JOptionPane.showMessageDialog(null, "You do not have enough money for that!");
                        }
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Enjoy your stay.");
                }
            }
        }
        if(p1.inJail)
            return false;
        return true;
    }

    public static void doRoll(int ply)
    {
        if(!canRoll(ply))
            return;

        int roll = dice.getValue();
        JOptionPane.showMessageDialog(null, "Player " + ply + " you rolled a " + roll + "!");
        if(ply==1)
            p1.move(roll);
        if(ply==2)
            p2.move(roll);
        if(turn == 1)
            turn = 2;
        else
            turn = 1;

        System.out.println("Player 1 square: " + p1.getSquare().getName());
        System.out.println("Player 2 square: " + p2.getSquare().getName());
        GUI.update();
    }
}