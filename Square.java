import java.awt.*;

public class Square extends SquareType
{
    //All updates to the GUI will be handled via properties stored here
    public GUISquareInfo GUISquare;

    public boolean own;
    public Pieces p1, p2;

    public Square( String name, String tileType, boolean own, int value, int index)
    {
        this.name = name;
        this.tileType = tileType;
        this.own = own;
        this.value = value;

        /*
        GUISquare = Main.GUI.getSquareByIndex(index);

        if(tileType.equals("property")){
            GUISquare.Color = Color.blue;
        }
        else if(tileType.equals("jail")){
            GUISquare.Color = Color.black;
        }
        else if(tileType.equals("other")){
            GUISquare.Color = Color.red;
        }
        else if(tileType.equals("chance")){
            GUISquare.Color = Color.yellow;
        }
        else if(tileType.equals("tax")){
            GUISquare.Color = Color.green;
        }
        else{
            GUISquare.Color = Color.cyan;
        }

        GUISquare.Name = name;
        GUISquare.Label = name;
        GUISquare.Index = index;
        if(!tileType.equals("chance") && !tileType.equals("other"))
            GUISquare.SubLabel = "";
        else
            GUISquare.SubLabel = ((Integer)value).toString();
        GUISquare.repaint();*/
    }

    public boolean canOwn() { return own; }

    public int getValue() { return value; }

    public void setValue( int value ) { this.value = value; }

    public void buy( int owner )
    {
        if( own && this.owner == 0 )
        {
            if( owner == 1 )
            {
                if( Main.p1.money >= value )
                {
                    Main.p1.money -= value;
                    super.owner = owner;
                }
            }
        }
    }

    public void sell()
    {
        if( owner == 1 )
        { Main.p1Money += value; }

        else if( owner == 2 )
        { Main.p1Money += value; }

        super.owner = 0;
    }

    public String getName1()
    {
        if( p1 != null )
        { return p1.getName(); }

        else { return " "; }
    }

    public String getName2()
    {
        if( p2 != null )
        { return p2.getName(); }

        else{ return " "; }
    }

    public boolean isOwned() { return owner != 0; }

    public void setp1Piece( Pieces p ) {
        if(p != null)
            GUISquare.Piece1 = p.piece;
        else
            GUISquare.Piece1 = MonopolyGUI.ePiece.None;
        p1 = p;
    }
    public void setp2Piece( Pieces p ) {
        if(p != null)
            GUISquare.Piece2 = p.piece;
        else
            GUISquare.Piece2 = MonopolyGUI.ePiece.None;
        p2 = p;
    }

    public MonopolyGUI.ePiece getPiece1()
    {
        if(p1 != null)
            return p1.piece;
        return MonopolyGUI.ePiece.None;
    }

    public MonopolyGUI.ePiece getPiece2()
    {
        if(p2 != null)
            return p2.piece;
        return MonopolyGUI.ePiece.None;
    }

}