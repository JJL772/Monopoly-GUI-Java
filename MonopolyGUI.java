import org.w3c.dom.css.Rect;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.spi.AudioFileReader;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
import javax.sound.*;

public class MonopolyGUI
{
    private GUIPlayerInfo Player1Square;
    private GUIPlayerInfo Player2Square;

    enum ePlayer
    {
        PLAYER1,
        PLAYER2,
    }

    enum ePiece
    {
        None,
        Dog,
        Man,
        Hat,
        Car,
    }

    public static String toString(ePiece pc){
        switch(pc){
            case None:
                return "";
            case Dog:
                return "Dog";
            case Man:
                return "Man";
            case Hat:
                return "Hat";
            case Car:
                return "Car";
            default:
                return "";
        }
    }

    public static ePiece fromString(String str){
        switch(str){
            case "":
                return ePiece.None;
            case "Dog":
                return ePiece.Dog;
            case "Man":
                return ePiece.Man;
            case "Hat":
                return ePiece.Hat;
            case "Car":
                return ePiece.Car;
            default:
                return ePiece.None;
        }
    }

    class ExitGame_Listener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(JOptionPane.showConfirmDialog(GameWindow, "Are you sure you want to exit?", "Exit?", JOptionPane.YES_NO_OPTION) == 0)
                System.exit(0);
        }
    }

    class RestartGame_Listener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(JOptionPane.showConfirmDialog(GameWindow, "Are you sure you want to restart the game?", "Restart?", JOptionPane.YES_NO_OPTION) == 0)
            {
                //...
            }
        }
    }

    class RollButton1_Listener implements  ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e){
            if(Main.turn == 1){
                Main.doRoll(1);
            }else{
                JOptionPane.showMessageDialog(null, "It is not your turn!");
            }
        }
    }

    class RollButton2_Listener implements  ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e){
            if(Main.turn == 2){
                Main.doRoll(2);
            }else{
                JOptionPane.showMessageDialog(null, "It is not your turn!");
            }
        }
    }

    class ButtonExt extends Button{
        int i = 0;

        public void paint(Graphics g){
            update(g);
        }
        @Override
        public void update(Graphics g)
        {
            if(i == 1)
            {
                g.setColor(Color.red);
            }
            if(i == 2)
            {
                g.setColor(Color.blue);
            }
            g.fillRect(0,0,109,118);
            g.setColor(Color.black);
            g.drawLine(0,0,109,0);
            g.drawLine(0,0,0,118);
            g.drawLine(109,0,109,118);
            g.drawLine(0,118,109,118);
            FontMetrics met = g.getFontMetrics();
            int center = 109/2-Math.round(met.stringWidth("Player " + i + " click to roll.")/2);
            g.drawString("Player " + i + " click to roll", center, 118/2);
        }
    }

    /* Properties */
    private Frame GameWindow;
    private MenuBar GameMenuBar;
    private ArrayList<Menu> GameMenus;
    private ArrayList<MenuItem> FileMenuOptions;
    private ArrayList<MenuItem> OptionsMenuOptions;
    private HashMap<GUISquareInfo, Rectangle> BoardSquares;
    private GUISquareInfo[] SquareInfos;
    private final int SquareWidth = 90;
    private final int SquareHeight = 90;

    public MonopolyGUI()
    {
        SetupGUI();
    }

    MonopolyGUI(GUISquareInfo[] squares, boolean bHidden)
    {
        SquareInfos = squares;
        SetupGUI();
        if(!bHidden)
            Show();
    }

    public ArrayList<Canvas> Components = new ArrayList<Canvas>();

    /*
    Performs all the setup actions
     */
    private void SetupGUI()
    {
        GameMenus = new ArrayList<Menu>();
        FileMenuOptions = new ArrayList<MenuItem>();
        OptionsMenuOptions = new ArrayList<MenuItem>();

        GameWindow = new Frame("Monopoly");
        GameWindow.setVisible(false);
        GameWindow.setSize(880, 1000);
        GameWindow.setResizable(false);
        //GameWindow.setBackground(Color.black);

        GameMenuBar = new MenuBar();
        GameWindow.setMenuBar(GameMenuBar);

        Menu FileMenu = new Menu("Game");
        GameMenus.add(FileMenu);
        GameMenuBar.add(FileMenu);

        Menu OptionsMenu = new Menu("Options");
        GameMenus.add(OptionsMenu);
        GameMenuBar.add(OptionsMenu);

        MenuItem RestartGameOption = new MenuItem("Restart");
        RestartGameOption.addActionListener(new RestartGame_Listener());
        FileMenu.add(RestartGameOption);

        MenuItem ExitGameOption = new MenuItem("Quit");
        ExitGameOption.addActionListener(new ExitGame_Listener());
        FileMenu.add(ExitGameOption);

        BoardSquares = new HashMap<GUISquareInfo, Rectangle>();

        GridLayout layout = new GridLayout(8,8);
        GameWindow.setLayout(layout);

        int i = 0;
        for(int x = 0; x < 8; x++){
            for(int y = 0; y < 8; y++){
                GUISquareInfo square = new GUISquareInfo("Test", Color.blue, "Mowry ff", "$200", x + y);
                if(Arrays.asList(SquareMappings).contains(i)){
                    int index = Arrays.asList(SquareMappings).indexOf(i);
                    Square s = Main.board[index];
                    square.BoardSquare = s;
                    square.Name = s.name;
                    square.Label = s.name;
                    square.Index = index;

                    if(s.tileType.equals("property") || s.tileType.equals("tax"))
                        square.SubLabel = "$"+s.value;
                    else
                        square.SubLabel = "";
                    if(s.tileType.equals("property"))
                        square.Color = Color.blue;
                    else if(s.tileType.equals("tax"))
                        square.Color = Color.green;
                    else if(s.tileType.equals("other"))
                        square.Color = Color.gray;
                    else if(s.tileType.equals("jail"))
                        square.Color = Color.black;
                    else if(s.tileType.equals("chance"))
                        square.Color = Color.yellow;
                    else
                        square.Color = Color.gray;
                    s.GUISquare = square;
                    Components.add(square);
                }

                if(x == 0 || x == 7) {
                    square.setVisible(true);
                }else{
                    if(y == 0 || y == 7){
                        square.setVisible(true);
                    }
                }

                if(y == 1 && x == 1){
                    GUIPlayerInfo info = new GUIPlayerInfo("Player 1");
                    GameWindow.add("Player1Info",info);
                    Player1Square = info;
                    info.setVisible(true);
                    Components.add(info);
                }
                else if(y == 1 && x == 2) {
                    GUIPlayerInfo info = new GUIPlayerInfo("Player 2");
                    GameWindow.add("Player2Info", info);
                    Player2Square = info;
                    info.setVisible(true);
                    Components.add(info);

                }
                else if(y == 3 && x == 1) {
                    ButtonExt button = new ButtonExt();
                    button.setLabel("Player 1 Roll");
                    button.i = 1;
                    button.addActionListener(new RollButton1_Listener());
                    GameWindow.add(button);
                }else if(y == 4 && x == 1){
                    ButtonExt button = new ButtonExt();
                    button.setLabel("Player 2 Roll");
                    button.addActionListener(new RollButton2_Listener());
                    button.i = 2;
                    GameWindow.add(button);
                }
                else{
                    GameWindow.add(((Integer) (x + y)).toString(), square);
                }

                //10,11
                i++;
            }
        }
    }

    /*
    Warning: potentially unsafe, if component #10 is NOT GUIPlayerInfo, there will be an issue with casting!
     */
    public GUIPlayerInfo getPlayer1InfoSquare(){
        return (GUIPlayerInfo)GameWindow.getComponent(10);
    }
    /*
    Warning: potentially unsafe, if component #10 is NOT GUIPlayerInfo, there will be an issue with casting!
     */
    public GUIPlayerInfo getPlayer2InfoSquare(){
        return (GUIPlayerInfo)GameWindow.getComponent(18);
    }



    /*
    This will actually display the GUI
     */
    public void Show()
    {
        GameWindow.setVisible(true);
    }

    /*
    Hides the GUI
     */
    public void Hide()
    {
        GameWindow.setVisible(false);
    }

    /*
    Forcibly redraws the GUI
     */
    public void update()
    {
        for(Canvas c : this.Components){
            c.repaint();

        }
    }

    public void setPlayer1Money(int money){
        Player1Square.Money = money;
    }

    public void setPlayer2Money(int money){
        getPlayer2InfoSquare().Money = money;
    }

    public void addPlayer1Property(){
        getPlayer1InfoSquare().OwnedPlaces++;
    }

    public void addPlayer2Property(){
        getPlayer2InfoSquare().OwnedPlaces++;
    }

    public void removePlayer1Property(){
        if(getPlayer1InfoSquare().OwnedPlaces <= 0)
            getPlayer1InfoSquare().OwnedPlaces = 0;
        else
            getPlayer1InfoSquare().OwnedPlaces--;
    }

    public void removePlayer2Property(){
        if(getPlayer2InfoSquare().OwnedPlaces <= 0)
            getPlayer2InfoSquare().OwnedPlaces = 0;
        else
            getPlayer2InfoSquare().OwnedPlaces--;
    }

    public void updatePlayerStates()
    {

    }

    //for getting squares by index
    static Integer[] SquareMappings = {
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            15,
            23,
            31,
            39,
            47,
            55,
            63,
            62,
            61,
            60,
            59,
            58,
            57,
            56,
            48,
            40,
            32,
            24,
            16,
            8,
    };

    public GUISquareInfo getSquareByIndex(int index)
    {
        if(index < 28 && index > 0)
            return (GUISquareInfo) GameWindow.getComponent(SquareMappings[index]);
        else
            return null;
    }
}