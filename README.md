# Monopoly

This is another Java project that was written for the AP Computer science class at my high school.
Like the checkers game, the GUI was built using Java swing.

Unfortunately, the program has a few bugs (probably due to weird quirks with java swing).
The GUI currently doesn't update properly after a move is run, and it requires that you actually click a game square in order for the GUI to update.
The board is created by using a grid layout that is then selectively filled with buttons, which represent the actual game squares.
Luckily, swing allows you to draw stuff on top of buttons, so pieces are rendered using that technique.

The piece class was provided by the instructor.